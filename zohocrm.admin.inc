<?php
/**
 * @file
 * Admin page for the ZohoCRM module
 */

/**
 * Implements hook_form().
 *
 * The basic authentication required to use the ZohoCRM API
 */
function zohocrm_admin_form($form, $form_state) {
  $form['zohocrm_authtoken'] = array(
    '#type' => 'textfield',
    '#title' => t('Authentication token'),
    '#default_value' => variable_get('zohocrm_authtoken', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => t('Generate your token as per the instructions listed on https://www.zoho.com/crm/help/api/using-authentication-token.html'),
  );

  $form['zohocrm_scope'] = array(
    '#type' => 'textfield',
    '#title' => t('Scope'),
    '#default_value' => variable_get('zohocrm_scope', 'crmapi'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => t('This is the scope you generated your authentication key using, generally this will not change from crmapi'),
  );

  $test_results = drupal_http_request('https://crm.zoho.com/crm/private/json/Users/getUsers?authtoken=' . variable_get('zohocrm_authtoken', '') . '&scope=' . variable_get('zohocrm_scope', 'crmapi') . '&type=AllUsers');

  $form['zohocrm_test'] = array(
    '#markup' => '<h3>Test: getUsers</h3>' . $test_results->data,
  );

  return system_settings_form($form);
}
