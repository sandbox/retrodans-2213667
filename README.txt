## About
This module has been 



## Installation
1. Download and enable the module
2. Go to the config page (admin/config/people/zohocrm) and insert your Zoho 
settings
3. Enable the plugin modules you would like to use (eg. webform)



## Configuration
1. Enable ZohoCRM and configure (as above)
2. Go to the webform admin page (admin/config/people/zohocrm/webform)
3. Choose your webform and enable
4. Map your webform fields to ZohoCRM fields
5. Click Save
6. Test in detail - let me know of fringe cases that break it (testing has 
been limited)



## Planned Improvements
* ability to add multiple webforms
* connect to contact-us form



## Troubleshooting
### New ZohoCRM lead sent via webform: 4401Unable to populate data, please check if mandatory value is entered correctly.
The form all stopped working, so I had to remove check_plain from the $field_mappings variable on webform.
It is also worth checking you have actually mapped things correctly



## Adding page_title as hidden field
Note this only works with a field called 'page_title' at the moment, to use with other fieldnames you may need to modify your own HOOK_form_alter()
* Set webform to use blocks
* insert block onto page
* create a field called page_title and insert the token %title into it and set to 'hidden field' (not 'secure value')
* flush the page cache
* Go to /admin/config/people/zohocrm/webform and set the field to push the data to (I chose 'Description')
* Go to the page the block is on, and click the edit cog
* select the 'Show all webform pages in block' checkbox and click save
* It should now all work nicely



## Changes to make
The client wants to be able to have the above setting default to 'webform' unless they are on 'contact us'.  Therefore, suggested solution is to create a new custom token called %zoho-title and put the logic on that to do a quick match on whether the user is on the contact us page. 



# UPDATES
## 7th June 2014 - Adding multi-webform capabilities
You will need to go into each of the admin pages and reconfigure once you update the module, as there was not time to write an upgrade path.  Please also test on a non production site as could only test myself against my personal Zoho settings, and a custom site.

In the menu there is a new menu setup, so each form has it's own admin page, the old admin page is still there, but is only used to enable the webforms as required.  After checking a new webform to manage, you may need to flush your cache to have it appear there.
Due to limited time, testing was minimal, but I hae 2 forms locally, and they seem to pull in the content once posted to the site

'''Setup process'''
* Update module
* flush cache
* goto: admin/config/people/zohocrm/webform
* Enable your forms
* Flush cache again (sorry about this, ran out of time to put in an auto flush of the menu)
* Use the menu to edit the config of your form (likely url is admin/config/people/zohocrm/webform/1)
* Test your form