<?php
/**
 * @file
 * Admin page for the ZohoCRM_webform module
 */

/**
 * Implements hook_form().
 *
 * Gets a list of the webforms for the user to choose from.
 * Then gets all the fields for that form, creating a textfield to allow mapping
 */
function zohocrm_webform_admin_form($form, $form_state) {
  // Get all webforms.
  $webform_types = webform_variable_get('webform_node_types');
  $nodes = array();
  if ($webform_types) {
    $nodes = db_select('node', 'n')
      ->fields('n')
      ->condition('n.type', $webform_types, 'IN')
      ->execute()
      ->fetchAllAssoc('nid');
  }
  $forms = array();
  foreach ($nodes as &$node) {
    $forms[$node->nid] = $node->title;
  }

  $form['zohocrm_webform_list'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Webform Name'),
    '#options' => $forms,
    '#default_value' => variable_get('zohocrm_webform_list', 0),
    '#description' => t('Choose which webform you would like ZohoCRM to connect to'),
  );

  return system_settings_form($form);
}


/**
 * Implements hook_form_validate().
 *
 * Creates a mappings variable in the DB to be used later
 */
function zohocrm_webform_admin_form_validate($form, &$form_state) {
  /*
  // Get all the fields for this webform.
  $node = db_select('webform_component', 'wc')
    ->fields('wc')
    ->condition('wc.nid', variable_get('zohocrm_webform_list', 0), '=')
    ->execute()
    ->fetchAllAssoc('form_key');

  // Use the variables supplied to create a mappings variable.
  $field_mappings = array();
  foreach ($form_state['values'] as $key => &$value) {
    if (substr($key, 0, 23) === 'zohocrm_webform_fields_') {
      $field_id = $node[substr($key, 23)]->cid;
      $field_name = substr($key, 23);
      $field_mapping = $value;
      $field_mappings[$field_id] = array(
        'cid' => $field_id,
        'fieldname' => $field_name,
        'zohomapping' => $field_mapping,
      );
    }
  }
  */

  // Create variable of mappings.
  //variable_set('zohocrm_webform_mappings', serialize($field_mappings));
}



/**
 *
 */
function zohocrm_webform_settings_form($form, $form_state) {
  // Get webform nid
  $nid = arg(5);

    // Get all the fields for this webform.
    $webform = db_select('webform_component', 'wc')      
      ->fields('wc')
      ->condition('wc.nid', $nid, '=')
      ->execute()
      ->fetchAllAssoc('form_key');

  $field_mappings = _get_mappings();
  
  //dpm($node);
  $node = node_load($nid);
  $form['contact_information'] = array(
    '#markup' => t('<h2>Webform settings for: ' . $node->title . '</h2>'),
  );

  // Create simple array of fields.
  $form['zohocrm_webform_type'] = array(
    '#type' => 'select',
    '#title' => t('ZohoCRM contact type'),
    '#options' => array('lead'),
    '#default_value' => (isset($field_mappings[$nid]['zohocrm_webform_type']) ? $field_mappings[$nid]['zohocrm_webform_type'] : '0'),
    '#description' => t('what type of contact would you like this to appear as in ZohoCRM?'),
  );
  $form['zohocrm_webform_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Webform fields'),
  );

  // Loop through the formfields, setting up partner ZohoMapping fields.
  foreach ($webform as $field_id => &$field) {
    $form['zohocrm_webform_fields']['zohocrm_webform_fields_' . $field_id] = array(
      '#type' => 'textfield',
      '#title' => check_plain(t("@id", array('@id' => $field_id))),
      '#default_value' => (isset($field_mappings[$nid][$field->cid]['zohomapping']) ? $field_mappings[$nid][$field->cid]['zohomapping'] : 'n/a'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#description' => t('Which CRM field should @field_id map to (if any) <a href="@info_link">Look here for examples</a>', array('@field_id' => $field_id, '@info_link' => 'https://www.zoho.com/crm/help/api/insertrecords.html#Insert_records_into_Zoho_CRM_from_third-party_applications')),
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Update configuration'));

  return $form;
}


function zohocrm_webform_settings_form_validate($form, $form_state){
  $nid = arg(5);

  // Get all the fields for this webform.
  $node = db_select('webform_component', 'wc')
    ->fields('wc')
    ->condition('wc.nid', $nid, '=')
    ->execute()
    ->fetchAllAssoc('form_key');

  $field_mappings = _get_mappings();
  
  foreach ($form_state['values'] as $key => &$value) {
    // Set the field mapping variables
    if (substr($key, 0, 23) === 'zohocrm_webform_fields_') {
      $field_id = $node[substr($key, 23)]->cid;
      $field_name = substr($key, 23);

      $field_mappings[$nid][$field_id] = array(
        'cid' => $field_id,
        'fieldname' => $field_name,
        'zohomapping' => $value,
      );
    }else if($key === 'zohocrm_webform_type'){
      // Set the webform type
      $field_mappings[$nid]['zohocrm_webform_type'] = $value;
    }
  }

  // Create variable of mappings.
  variable_set('zohocrm_webform_mappings', serialize($field_mappings));
}



